from fverificador import CVerificador

matriz = [[1,1,1],[1,2,1],[1,2,1]]
miTablero = CVerificador()

print(miTablero.verificarFilas(matriz))
print(miTablero.verificarColumnas(matriz))
print(miTablero.verificarDiagonales(matriz))